include ../common/common.mak

OBJS=$(OBJ)/mkrand.o
TARGET=$(BIN)/mkrand

$(TARGET): $(OBJS)

clean:
	rm -f $(OBJS) $(TARGET) $(TARGET).exe
