# folder definitions
SRC=.
OBJ=./obj
BIN=./bin
COM_SRC=../common

# configuration variables
CC=gcc
LD=gcc

CFLAGS=-Wextra -Wall -I$(SRC) -I$(COM_SRC)
LDFLAGS=



# generic rules
$(OBJ)/%.o: $(SRC)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ -c $<

$(OBJ)/%.o: $(COM_SRC)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BIN)/%:
	@mkdir -p $(@D)
	$(LD) -o $@ $^
