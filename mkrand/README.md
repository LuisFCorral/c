# mkrand

## Requirements

1. Read `NUM` enviroment variable.
2. Print the specified quantity of random numbers to the stdout.

> see [printf](https://linux.die.net/man/3/printf), [rand](https://linux.die.net/man/3/rand), [atoi](https://linux.die.net/man/3/atoi), [getenv](https://linux.die.net/man/3/getenv)

## Expected results

```
mkrand $ export NUM=2
mkrand $ bin/mkrand
23
102
mkrand $ NUM=4 bin/mkrand
3
12
50
5
```
