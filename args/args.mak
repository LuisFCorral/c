include ../common/common.mak

OBJS=$(OBJ)/args.o
TARGET=$(BIN)/args

$(TARGET): $(OBJS)

clean:
	rm -f $(OBJS) $(TARGET) $(TARGET).exe
