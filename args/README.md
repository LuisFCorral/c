# args

## Requirements

1. Print each supplied command line argument to the screen on separete lines.

## Expected results

```
args $ bin/args 1 2 3
1
2
3
args $ bin/args 1 "2 3"
1
2 3
args $ bin/args '1 "2 3"'
1 "2 3"
args $ export VAR="1 2"
args $ bin/args 1 $VAR
1
2
3
args $ bin/args 1 "$VAR"
1
2 3
args $ bin/args 1 '$VAR'
1
$VAR
```
